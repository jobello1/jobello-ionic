import { ProProfile } from 'src/model/pro_profile';
import { StudentProfile } from 'src/model/student_profile';

export class Request {
    id:             number;
    date:           string;
    sender:         StudentProfile;
    contact:        ProProfile;
    message:        string;
    isAccepted:     boolean;

    constructor(id: number, date: Date, sender: StudentProfile, contact: ProProfile, isAccepted: boolean) {
        this.id         = id;
        this.date       = date.getHours()+":"+date.getMinutes();
        this.sender     = sender;
        this.contact    = contact;
        this.isAccepted = isAccepted;

        if(isAccepted)
            this.message = "Votre demande de contact a été accepté !";
        else
            this.message = "Demande de contact envoyée le "+date.getDate()+"/"+(date.getMonth()+1);
    }
}