import { ProProfile } from './pro_profile';
import { Profile } from './profile';

export class Answer{
    id: number;
    content: string;
    author: ProProfile;
    creationDate: Date;

    public constructor(id: number, content: string, author: ProProfile,creationDate: Date){
        this.id = id;
        this.content = content;
        this.author = author;
        this.creationDate = creationDate;
    }

}