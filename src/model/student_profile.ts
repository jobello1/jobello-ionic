import { Profile } from './profile';
import { ProfileNote } from './profile_note';

export class StudentProfile extends Profile {

    formation: string;

    public constructor (id: number,name: string,firstname: string,birthdate: Date,pictureUrl: string, descriptionNote: ProfileNote,formation: string){
        super(id,Role.student,name,firstname,birthdate,pictureUrl,descriptionNote);
        this.formation = formation;
    }
}