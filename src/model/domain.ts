import { Job } from './job';

export class Domain{

    id: number;
    title: string;
    imageUrl: string;
    jobs: Array<Job>;
    color: string;
    textColor: string;

    public constructor( id: number, title: string, imageUrl: string, jobs: Array<Job>, color: string, textColor: string){
        this.id = id;
        this.title = title;
        this.imageUrl = imageUrl;
        this.jobs = jobs;
        this.color = color;
        this.textColor = textColor;
    }
}