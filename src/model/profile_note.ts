export class ProfileNote{
    title: string;
    text: string;

    public constructor(title:string, text: string){
        this.title = title;
        this.text = text;
    }
}