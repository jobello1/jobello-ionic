import { Profile } from 'src/model/profile';

export class Message {
    content:            string;
    profileSender:      Profile;
    profileContact:     Profile;
    contact:            string; 

    constructor(content: string, profileSender: Profile, profileContact: Profile, contact: string) {
        this.content            = content;
        this.profileSender      = profileSender;
        this.profileContact     = profileContact;
        this.contact            = contact;
    }
}