import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { Job } from './job';
import { Mail } from './mail';
import { Domain } from './domain';
import { Answer } from './answer';
import { Question } from './question';
import { Message } from './message';
import { Formation } from './formation.enum';
import { Request } from './request';

import { ProProfile } from './pro_profile';
import { ProfileNote } from './profile_note';
import { StudentProfile } from './student_profile';

@Injectable({ providedIn: 'root' })
export class Data {

    role = Role.student;
    token = "";
    roleSubject = new BehaviorSubject<Role>(Role.student);
    public roleObservable = this.roleSubject.asObservable();


    jobs = [
        new Job(0, 'Assistant commercial', 'https://cdn.pixabay.com/photo/2016/01/19/17/53/writing-1149962_960_720.jpg','L\'assistant commercial permet la communication entre les différents acteurs de son service, les commerciaux, les clients et la direction.',[new Job(2, 'Directeur financier', '','',[],[]), new Job(3, 'Ingénieur commercial', '','',[],[])],['Conseiller Commercial']),
        new Job(1, 'Comptable', 'https://cdn.pixabay.com/photo/2016/01/19/17/53/writing-1149962_960_720.jpg','Une brève description du métier de comptable. Le but est de donner une idée rapide du métier.',[],[]),
        new Job(2, 'Conseiller commercial', 'https://cdn.pixabay.com/photo/2016/01/19/17/53/writing-1149962_960_720.jpg','Une brève description du métier de conseiller commercial. Le but est de donner une idée rapide du métier.',[],['Assistant Commercial']),
        new Job(3, 'Directeur financier', 'https://cdn.pixabay.com/photo/2017/09/14/11/50/women-2748752_960_720.jpg','Une brève description du métier. Le but est de donner une idée rapide du métier.',[],[]),
        new Job(4, 'Ingénieur commercial', 'https://cdn.pixabay.com/photo/2019/06/06/16/02/technology-4256272_960_720.jpg','Une brève description du métier. Le but est de donner une idée rapide du métier.',[],[]),
        new Job(5, 'Juriste', 'https://cdn.pixabay.com/photo/2015/09/20/15/46/binding-contract-948442_960_720.jpg','Une brève description du métier. Le but est de donner une idée rapide du métier.',[],[]),
    ];

    domains = [
        new Domain(0, 'Finances', 'https://cdn.pixabay.com/photo/2016/11/27/21/42/stock-1863880_960_720.jpg', [this.jobs[0],this.jobs[1],this.jobs[2],this.jobs[3],this.jobs[4]], '#7431cc', '#ffffff'),
        new Domain(1, 'Informatique', 'https://cdn.pixabay.com/photo/2016/03/09/09/17/computer-1245714_960_720.jpg', [], '#001b66', '#ffffff'),
        new Domain(2, 'Nature', 'https://cdn.pixabay.com/photo/2017/05/31/18/38/sea-2361247_960_720.jpg', [], '#08bd02', '#ffffff'),
        new Domain(3, 'Bricolage', 'https://cdn.pixabay.com/photo/2014/10/22/16/39/tools-498202_960_720.jpg', [], '#4f4f4f', '#ffffff'),
        new Domain(4, 'Automobile', 'https://cdn.pixabay.com/photo/2016/11/29/10/01/vw-bully-1868890_960_720.jpg', [], '#c8cf00', '#ffffff'),
        new Domain(5, 'Aviation', 'https://cdn.pixabay.com/photo/2017/10/01/16/25/aircraft-2806035_960_720.jpg', [], '#a10008', '#ffffff'),
        new Domain(6, 'Militaire', 'https://cdn.pixabay.com/photo/2015/09/04/07/19/trumpeters-921709_960_720.jpg', [], '#693178', '#ffffff'),
        new Domain(7, 'Social', 'https://cdn.pixabay.com/photo/2016/03/09/09/22/workplace-1245776_960_720.jpg', [], '#ff70c1', '#ffffff'),
        new Domain(8, 'Sport', 'https://cdn.pixabay.com/photo/2017/04/12/10/08/handstand-2224104_960_720.jpg', [], '#e36002', '#ffffff'),
        new Domain(9, 'Astronomie', 'https://cdn.pixabay.com/photo/2015/03/26/09/59/purple-690724_960_720.jpg', [], '#7b3782', '#ffffff'),
    ]

    studentProfiles = [
        new StudentProfile(
            0,
          'Sophie',
          'Delart',
          new Date('01/02/2005'),
          'https://cdn.pixabay.com/photo/2018/12/29/16/06/girl-3901714_960_720.jpg',
          new ProfileNote('A propos de Sophie', 'Hello moi c\'est Sophie. Je suis passionée de danse et de voyage. J\'aimerais trouver un métier qui me permette de découvrir d\'autres pays et culture.'),
          Formation.premiere_es
        )
    ];

    professionalProfiles = [
        new ProProfile(
            0,
            "Constant","Alain",new Date('04/06/1986'),
            "https://miro.medium.com/max/10368/1*Zi7t4KAPyvFgnDs1Uojyag.jpeg",
            new ProfileNote("Ma vision des choses","L'énergie positive apporte des choses positives ! Mon métier d'assistant commercial est une véritable source d'inspiration que je serais heureux de partager avec vous."),
            "Hauts-de-France",
            "Assistant commercial"
        ),
    ];

    answers = [
        new Answer(
            0,
            "Le métier d'assistant commercial consiste à assurer le bon fonctionnement du commerce au sein d'une agence. Le poste peut avoir plusieurs facettes comme rédiger et remplir des documents officiels (actes de vente, bails) mais aussi assurer une bonne communication entre les commerciaux.",
            this.professionalProfiles[0],
            new Date()
        )

    ];

    

    questions = [
        new Question(
            0,
            'Quel est le rôle d\'une assistante commercial ?',
            'Je n\'ai pas compris le rôle précis d\'une assistante commercial au sein d\'une entreprise. Son poste semble ressembler à celui d\'une commerciale mais sans le contant avec le client. Est-ce le cas ?',
            this.studentProfiles[0],
            new Date(),
            this.jobs[0],
            this.answers[0],
        ),new Question(
            1,
            'Est-ce compliqué de trouver un emploi dans une agence immobilière ? ',
            'Je voudrais devenir assistant commercial mais j\' ai peur de ne pas trouver de poste.',
            this.studentProfiles[0],
            new Date(),
            this.jobs[0],
            null
        )
    ];

    myQuestions = [
       this.questions[0],
       this.questions[1]
    ];

    myFavoriteJobs = [];

    mails = [];

    requests = [];

    acceptRequest(request: Request) {
        this.requests.push(new Request(request.id, new Date(), request.sender, request.contact, true));
        localStorage.setItem('requests', JSON.stringify(this.requests));
    }
    addRequest(request: Request) {
        this.requests.push(request);
        localStorage.setItem('requests', JSON.stringify(this.requests));
    }

    deleteRequest(request: Request) {
        this.requests.splice(request.id, 1);
        localStorage.setItem('requests', JSON.stringify(this.requests));
    }

    lenghtRequest() {
        return this.requests.length;
    }

    getRole() {
        if ( localStorage.getItem('role') ) {
            this.role = JSON.parse(localStorage.getItem('role'));
        }
        this.roleSubject.next(this.role);
        return this.role;
    }

    setRole(role: Role){
        this.role = role;
        this.roleSubject.next(role);
        localStorage.setItem('role', JSON.stringify(this.role));
    }

    getDomains() {
        if ( localStorage.getItem('domains') ) {
            this.domains = JSON.parse(localStorage.getItem('domains'));
        }
        return this.domains;
    }

    getJobs() {
        if ( localStorage.getItem('jobs') ) {
            this.jobs = JSON.parse(localStorage.getItem('jobs'));
        }
        return this.jobs;
    }

    getCurrentStudentProfile() {
        return this.getStudentProfiles()[0];
    }

    getCurrentProfessionalProfile() {
        return this.getProfessionalProfiles()[0];
    }

    getProfessionalProfile(id: number) {
        return this.getProfessionalProfiles()[id];
    }

    getProfessionalProfiles() {
        if ( localStorage.getItem('professionalProfiles') ) {
            this.professionalProfiles = JSON.parse(localStorage.getItem('professionalProfiles'));
        }
        return this.professionalProfiles;
    }

    getStudentProfile(id: number) {
        return this.getStudentProfiles()[id];
    }

    getStudentProfiles() {
        if ( localStorage.getItem('studentProfiles') ) {
            this.studentProfiles = JSON.parse(localStorage.getItem('studentProfiles'));
        }
        return this.studentProfiles;
    }

    getMail(id: number) {
        var mailToReturn = this.mails[1];
        this.getMails().forEach(curr => {
            if(curr.id == id)
               mailToReturn = curr;
        });
        return mailToReturn;
    }

    getMails(){
        if(localStorage.getItem('mails')) {
            this.mails = JSON.parse(localStorage.getItem('mails'));
        }
        return this.mails;
    }

    setMails(mail: Mail){
        this.mails[mail.id] = mail;
        localStorage.setItem('mails', JSON.stringify(this.mails));
    }

    getMyQuestions() {
        if ( localStorage.getItem('myQuestions') ) {
            this.myQuestions = JSON.parse(localStorage.getItem('myQuestions'));
        }
        return this.myQuestions;
    }

    getMyFavoriteJobs() {
        if ( localStorage.getItem('myFavoriteJobs') ) {
            this.myFavoriteJobs = JSON.parse(localStorage.getItem('myFavoriteJobs'));
        }
        return this.myFavoriteJobs;
    }

    setmyFavoriteJobs(jobs: Job[]){
        this.myFavoriteJobs = jobs;
        localStorage.setItem('myFavoriteJobs', JSON.stringify(this.myFavoriteJobs));
        console.log(this.myFavoriteJobs);
    }

    setMyQuestions(questions: Question[]){
        this.myQuestions = questions;
        localStorage.setItem('myQuestions', JSON.stringify(this.myQuestions));
        console.log(this.myQuestions);
    }

    setQuestions(questions: Question[]){
        this.questions = questions;
        localStorage.setItem('questions', JSON.stringify(this.questions));
        console.log(this.questions);
    }

    setProProfile(profile: ProProfile) {
        this.professionalProfiles[profile.id] = profile;
        localStorage.setItem('professionalProfiles', JSON.stringify(this.professionalProfiles));
    }

    setStudentProfile(profile: StudentProfile) {
        this.studentProfiles[profile.id] = profile;
        localStorage.setItem('studentProfiles', JSON.stringify(this.studentProfiles));
    }

    getQuestions(){
        if ( localStorage.getItem('questions') ) {
           this.questions = JSON.parse(localStorage.getItem('questions'));
        }
        return this.questions;
    }
    getRequests() {
        if(localStorage.getItem('requests'))
            return JSON.parse(localStorage.getItem('requests')).reverse();
        else
            return this.requests.reverse();
    }

    getToken() {
        if(localStorage.getItem('token')) {
            return localStorage.getItem('token');
        }
        return "";
    }

    setToken(token: string) {
        this.token = token;
        localStorage.setItem('token', token);
    }

    save() {
        localStorage.setItem('jobs', JSON.stringify(this.jobs));
        localStorage.setItem('studentProfiles', JSON.stringify(this.studentProfiles));
        localStorage.setItem('professionalProfiles', JSON.stringify(this.professionalProfiles));
        localStorage.setItem('myQuestions', JSON.stringify(this.myQuestions));
    }

}