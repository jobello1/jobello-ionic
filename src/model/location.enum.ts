export enum Location {
    auvergne=   "Auvergne-Rhône-Alpes",
    bourgogne=  "Bourgogne-Franche-Comté",
    bretagne=   "Bretagne",
    centre=     "Centre-Val de Loire",
    corse=      "Corse",
    est=        "Grand Est",
    nord=       "Hauts-de-France",
    ile=        "Île-de-France",
    normandie=  "Normandie",
    aquitaine=  "Nouvelle-Aquitaine",
    occitanie=  "Occitanie",
    loire=      "Pays de la Loire",
    provence=   "Provence-Alpes-Côte d'Azur"
}