import { Job } from './job';

import { Answer } from './answer';
import { Profile } from './profile';
import { StudentProfile } from './student_profile';

export class Question{
    id: number;
    title: string;
    content: string;
    author: StudentProfile;
    creationDate: Date;
    answer: Answer;
    job: Job;

    public constructor (id: number,title:string, content:string, author: StudentProfile, creationDate: Date, job: Job, answer: Answer){
        this.id = id;
        this.title = title;
        this.content = content;
        this.author = author;
        this.creationDate = creationDate;
        this.answer = answer;
        this.job = job;
    }
}