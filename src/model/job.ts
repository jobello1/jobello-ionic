export class Job{

    id: number;
    title: string;
    imageUrl: string;
    shortDescription: string;
    linkedJobs: Array<Job>;
    synonyms : Array<string>;

    public constructor( id: number, title: string, imageUrl: string,shortDescription: string,linkedJobs: Array<Job>, synonyms : Array<string>){
        this.id = id;
        this.title = title;
        this.imageUrl = imageUrl;
        this.shortDescription = shortDescription;
        this.linkedJobs = linkedJobs;
        this.synonyms = synonyms;
    }
}