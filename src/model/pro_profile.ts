import { Profile } from './profile';
import { ProfileNote } from './profile_note';

export class ProProfile extends Profile{
    location : string;
    job: string;
    optionalNotes: Array<ProfileNote>;

    public constructor(id: number,name: string,firstname: string,birthdate: Date,pictureUrl: string, descriptionNote: ProfileNote,location: string,job:string){
        super(id,Role.pro,name,firstname,birthdate,pictureUrl,descriptionNote);
        this.location = location;
        this.job = job;
        this.optionalNotes = this.optionalNotes;
    }
}