import { ProfileNote } from './profile_note';

export abstract class Profile{
    public id: number;
    public role: Role;
    public name: string;
    public firstname: string;
    public birthdate: Date;
    public pictureUrl: string;
    public descriptionNote: ProfileNote;

    constructor(id: number, role: Role, name: string,firstname: string,birthdate: Date,pictureUrl: string, descriptionNote: ProfileNote){
        this.id = id;
        this.role = role;
        this.name = name;
        this.firstname = firstname;
        this.birthdate = birthdate;
        this.pictureUrl = pictureUrl;
        this.descriptionNote = descriptionNote;
    }
}