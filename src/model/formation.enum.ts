export enum Formation {
    seconde=        "Seconde",
    premiere_s=     "Première S",
    premiere_es=    "Première ES",
    premiere_l=     "Première L",
    terminale_s=    "Terminale S",
    terminale_es=   "Terminale ES",
    terminale_l=    "Terminale L"
}