import { ProProfile } from 'src/model/pro_profile';
import { Message } from 'src/model/message';

export class Mail {
    id:                 number;
    contact:            ProProfile;
    messages:           Message[];
    lastMessageTime:    string;

    public constructor(id: number, contact: ProProfile, messages: Message[], lastMessageTime: Date) {
        this.id                 = id;
        this.contact            = contact;
        this.messages           = messages;
        this.lastMessageTime    = lastMessageTime.getHours()+":"+lastMessageTime.getMinutes();
    }
}