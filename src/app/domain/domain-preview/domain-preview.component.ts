import { Domain } from './../../../model/domain';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-domain-preview',
  templateUrl: './domain-preview.component.html',
  styleUrls: ['./domain-preview.component.scss'],
})
export class DomainPreviewComponent implements OnInit {

  @Input()
  domain: Domain;

  constructor() { }

  ngOnInit() {}

}
