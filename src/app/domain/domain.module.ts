import { DomainDetailComponent } from './domain-detail/domain-detail.component';
import { DomainPreviewComponent } from './domain-preview/domain-preview.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DomainPageRoutingModule } from './domain-routing.module';

import { DomainPage } from './domain.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DomainPageRoutingModule
  ],
  exports:[DomainPreviewComponent],
  declarations: [DomainPage, DomainPreviewComponent, DomainDetailComponent]
})
export class DomainPageModule {}
