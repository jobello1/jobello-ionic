import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DomainPage } from './domain.page';

describe('DomainPage', () => {
  let component: DomainPage;
  let fixture: ComponentFixture<DomainPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DomainPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DomainPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
