import { DomainDetailComponent } from './domain-detail/domain-detail.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DomainPage } from './domain.page';

const routes: Routes = [
  {
    path: '',
    component: DomainPage
  },
  {
    path: 'detail/:id',
    component: DomainDetailComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DomainPageRoutingModule {}
