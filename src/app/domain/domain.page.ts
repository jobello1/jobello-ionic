import { JobService } from './../../service/job.service';
import { Job } from './../../model/job';
import { NavController } from '@ionic/angular';
import { DomainService } from './../../service/domain.service';
import { Domain } from './../../model/domain';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-domain',
  templateUrl: './domain.page.html',
  styleUrls: ['./domain.page.scss'],
})
export class DomainPage implements OnInit {

  domains: Array<Domain>;
  selectedJob: Job;
  searchText: string;
  jobsToDisplay: Array<Job>;

  constructor(private domainService: DomainService, private navCtrl: NavController, private jobService: JobService) {}

  ngOnInit() {
    this.domains = this.domainService.getDomains();
  }

  navBack(){
    this.navCtrl.back();
  }

  searchJob(){
    if(this.searchText.length >= 3){
      let jobs = this.jobService.getJobs();
      let jobsToDisplay = new Array<Job>();
      jobs.forEach( job => {
        if ( job.title.toUpperCase().includes(this.searchText.toUpperCase()) ) {
          jobsToDisplay.push(job);
        }
      });
      this.jobsToDisplay = jobsToDisplay;
    }
  }

}
