import { Job } from './../../../model/job';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { DomainService } from './../../../service/domain.service';
import { Domain } from './../../../model/domain';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-domain-detail',
  templateUrl: './domain-detail.component.html',
  styleUrls: ['./domain-detail.component.scss'],
})
export class DomainDetailComponent implements OnInit {

  domain: Domain;
  domainId: number;
  private sub: any;
  jobsToDisplayByLetter: Map<string,Array<Job>>;
  displayDetails : Map<Job,boolean>;

  constructor(private navCtrl: NavController, private route: ActivatedRoute, private domainService: DomainService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.domainId = params['id'];
      this.domain = this.domainService.getDomain(this.domainId);
      this.jobsToDisplayByLetter = this.domainService.getDomainJobsMapByFirstLetter(this.domain);
      this.displayDetails = new Map<Job,boolean>();
      this.domain.jobs.forEach( job =>{
        this.displayDetails.set(job,false);
      });
    });

  }

  navBack(){
    this.navCtrl.back();
  }

  displayJobDetail(job: Job){
    this.displayDetails.set(job,!this.displayDetails.get(job));
  }
}
