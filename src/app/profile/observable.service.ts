import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

//import { IApiService } from 'src/service/api.service.interface';
import { IObservableServiceProfile } from './observable.service.interface';
import { Profile } from 'src/model/profile';

/**
 * Service managing observable objects.
 * (for items)
 */
@Injectable({
    providedIn: 'root'
})
export class ObservableServiceProfile implements IObservableServiceProfile {

    //constructor(private apiService: IApiService) {}

    getProfile(path: string, params?: HttpParams): Promise<Profile> {
        throw new Error("Method not implemented.");
        //return this.apiService.get<Profile>(path, params).then(this.extractData).catch(this.handleError);
    }

    /**
     * Extract JSON data from response.
     *
     * @returns JSON data
     */
    private extractData(res: any) {
        return res;
    }
    /**
     * Handle error.
     *
     * @returns exception
     */
    private handleError(error: Response | any) {
        return Observable.throw('Error calling the url');
    }
}