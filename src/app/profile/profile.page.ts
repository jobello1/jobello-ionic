import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, ToastController } from '@ionic/angular';

import { ProfileService } from 'src/service/profile.service';
import { UserService } from 'src/service/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  age:              number;
  profile:          any;
  editButtonTitle:  string;
  isStudent:        boolean;
  editView:         boolean;

  constructor(private navCtrl: NavController, private activatedRoute: ActivatedRoute, private toastController: ToastController, private profileService: ProfileService, private userService: UserService) { }
  
  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      var id    = params['id'];
      var role  = params['role'];

      if(id == undefined) {
        id        = 0;
        role      = this.userService.getRole();
        this.editView  = true;
      }
      
      if(role == Role.student){
        this.isStudent = true;
        this.profile = this.profileService.getStudentProfile(id);
      }else{
        this.isStudent = false;
        this.profile = this.profileService.getProProfile(id);
      }
      this.age = this.profileService.getAge(this.profile.birthdate);
    }); 

    this.editButtonTitle = "Modifier";
    this.activatedRoute.params.subscribe(params => {
      if(params['fromEdit'] != null)
        this.presentToast();
    });
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Votre profil a été modifié.',
      duration: 2000,
      showCloseButton: true,
      closeButtonText: 'Fermer'
    });
    toast.present();
  }

  goAnOtherPage() {
    this.navCtrl.navigateForward("/profile/edit");
  }
  navBack(){
    this.navCtrl.back();
  }

}
