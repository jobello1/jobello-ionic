import { HttpParams } from '@angular/common/http';

import { Profile } from 'src/model/profile';

export abstract class IObservableServiceProfile {

    /**
     * Get a profile
     */
    abstract getProfile(path: string, params?: HttpParams): Promise<Profile>;
}