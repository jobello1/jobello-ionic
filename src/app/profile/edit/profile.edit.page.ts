import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';

import { Formation } from 'src/model/formation.enum';
import { Location } from 'src/model/location.enum';

import { ProfileService } from 'src/service/profile.service';

@Component({
    selector: 'app-profile-edit',
    templateUrl: './profile.edit.page.html',
    styleUrls: ['./profile.edit.page.scss'],
  })
export class ProfileEditPage {

  /**
   * Student label
   */
  formationLabel:       string;

  /**
   * Pro labels
   */
  jobLabel:             string;
  locationLabel:        string;

   /**
    * Commun labels
    */
   backButtonLabel:     string;
   saveButtonLabel:     string;
   birthdayDateLabel:   string;

  allFormationsValues:  string[];
  allLocationsValues:   string[];

  profile:              any;
  isStudent:            boolean;

  constructor( private alertController: AlertController, private navCtrl: NavController, private profileService: ProfileService) {
    this.backButtonLabel      = "Modifier le profil";
    this.saveButtonLabel      = "Enregistrer";
    this.formationLabel       = "Formation :";
    this.birthdayDateLabel    = "Date de naissance : ";
    this.jobLabel             = "Métier : ";
    this.locationLabel        = "Région : ";

    this.allFormationsValues  = Object.values(Formation);
    this.allLocationsValues   = Object.values(Location);

    this.profile = this.profileService.getCurrentProfile();
    if(this.profile.role == Role.student)
      this.isStudent = true;
    else
      this.isStudent = false;
  }

  async alertConfirmCancel() {
    const alert = await this.alertController.create({
      message: 'Toutes les modifications non sauvegardées seront perdues !',
      buttons: [
        {
          text: 'Rester',
          role: 'cancel',
          cssClass: 'secondary',
        }, {
          text: 'Quitter',
          handler: () => {
            this.backToProfile();
          }
        }
      ]
    });

    await alert.present();
  }

  backToProfile(){
    this.navCtrl.navigateForward("/profile",{ replaceUrl: true });
  }

  birthdayChange(e) {
    this.profile.birthdate = new Date(Object.values(e.detail)[0].toString());
  }

  descriptionChange(e) {
    this.profile.descriptionNote.text = Object.values(e.detail)[0].toString();
  }

  formationChange(e) {
    this.profile.formation = Object.values(e.detail)[0].toString();
  }

  locationChange(e) {
    this.profile.location = Object.values(e.detail)[0].toString();
  }

  saveAndGoToProfile(){
    if(this.isStudent)
      this.profileService.setStudentProfile(this.profile);
    else
      this.profileService.setProProfile(this.profile);

    this.navCtrl.navigateForward("/profile/fromEdit");
  }

}