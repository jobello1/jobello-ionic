import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfilePage } from './profile.page';
import { ProfileEditPage } from './edit/profile.edit.page';

const routes: Routes = [
  
  {
    path: '',
    component: ProfilePage
  },
  {
    path: 'edit',
    component: ProfileEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfilePageRoutingModule {}
