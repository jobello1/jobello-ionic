import { FavoriteJobService } from './../../../service/favoriteJob.service';
import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { Job } from 'src/model/job';

@Component({
  selector: 'app-job-preview',
  templateUrl: './job-preview.component.html',
  styleUrls: ['./job-preview.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class JobPreviewComponent implements OnInit {

  @Input()
  job: Job;

  isFavorite: boolean;
  color: string;
  nonFavoriteColor = '';
  favoriteColor = 'primary';


  constructor(private favoriteJobService: FavoriteJobService) { }

  ngOnInit() {
    this.isFavorite = this.favoriteJobService.isJobInFavorite(this.job);
    console.log('job is favorite :'+this.isFavorite);
    this.updateFavoriteButtonColor();
  }

  private updateFavoriteButtonColor() {
    if (this.isFavorite) {
      this.color = this.favoriteColor;
    } else {
      this.color = this.nonFavoriteColor;
    }
  }

  switchFavorite(event){
    event.stopImmediatePropagation();
    event.stopPropagation();
    event.preventDefault();
    this.isFavorite = !this.isFavorite;
    this.updateFavoriteButtonColor();
    if(this.isFavorite){
      this.favoriteJobService.addJobToFavorite(this.job);
    } else {
      this.favoriteJobService.removeJobFromFavorite(this.job);
    }
  }

}
