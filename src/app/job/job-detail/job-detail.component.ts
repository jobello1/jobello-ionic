import { JobService } from './../../../service/job.service';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Job } from 'src/model/job';
import { NavController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-job-detail',
  templateUrl: './job-detail.component.html',
  styleUrls: ['./job-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class JobDetailComponent implements OnInit {

  job: Job;
  jobId: number;
  private sub: any;
  answerText: string;
  userCanAnswer: boolean;

  constructor(private jobService: JobService, private route: ActivatedRoute,  private navCtrl: NavController, private toastController: ToastController) { }

  ngOnInit() {
     this.sub = this.route.params.subscribe(params => {
      this.jobId = params['id'];
      this.job = this.jobService.getJob(this.jobId);
      console.log(this.job);
    });
    
  }

  navBack(){
    this.navCtrl.back();
  }
  displayJobQuestion(){
    this.navCtrl.navigateForward('/question/job/'+this.job.id);
  }

  displayPro(){
    this.presentToast("Cette fonctionnalité n'est pas encore disponible. Elle vous permettra de consulter la liste des professionnels de ce métier et d'entrer en contact avec eux.");
  }

  displayContact(){
    this.presentToast("Cette fonctionnalité n'est pas encore disponible. Elle vous permettra de demander à être contacter par n'importe quel professionnel du métier.");
  }

  //TODO: put all toast method in a service and call that service to display toast messages
  async presentToast(myMessage: string) {
    const toast = await this.toastController.create({
      message: myMessage,
      duration: 4000,
      showCloseButton: true,
      closeButtonText: 'Fermer'
    });
    toast.present();
  }
  

}
