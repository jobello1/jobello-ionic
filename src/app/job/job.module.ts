import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JobPageRoutingModule } from './job-routing.module';

import { JobPage } from './job.page';
import { JobDetailComponent } from './job-detail/job-detail.component';
import { JobPreviewComponent } from './job-preview/job-preview.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    JobPageRoutingModule
  ],
  declarations: [JobPage, JobDetailComponent, JobPreviewComponent],
  exports: [JobPreviewComponent]
})
export class JobPageModule {}
