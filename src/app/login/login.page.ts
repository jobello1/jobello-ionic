import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';

import { Login } from 'src/model/login';

import { ObservableServiceLogin } from './observable.service';
import { UserService } from 'src/service/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  inputMail:      string;
  inputPassword:  string;

  constructor(private navCtrl: NavController, private observableLogin: ObservableServiceLogin, private toastController: ToastController, private userService: UserService) { }

  ngOnInit() {
    this.userService.setToken("");
  }

  async presentToast(myMessage: string) {
    const toast = await this.toastController.create({
      message: myMessage,
      duration: 3000,
      showCloseButton: true,
      closeButtonText: 'Fermer'
    });
    toast.present();
  }

  sendLogin() {
    var instance = this;
    this.observableLogin.connection('http://51.91.159.187:8990/login/', new Login(this.inputMail, this.inputPassword)).then(function(data){
    var access_token;
      var stringToken = JSON.stringify(data.data);
      var splitToken = stringToken.split(",");
      splitToken.forEach(current => {
        if(current.includes("access_token")) {
          access_token = current.split(":")[1].toString();
        }
      });
      instance.presentToast(access_token.substring(2, access_token.length-2));
      instance.userService.setToken(access_token.substring(2, access_token.length-2));
      if(instance.inputMail.includes("stud")) {
        instance.switchToSchooler();
      }else if(instance.inputMail.includes("pro")){
        instance.switchToPro();
      }
    });
  }

  switchToSchooler() {
    this.userService.setRole(Role.student);
    this.navCtrl.navigateForward("/home/student");
  }

  switchToPro() {
    this.userService.setRole(Role.pro);
    this.navCtrl.navigateForward("/home/pro");
  }

}
