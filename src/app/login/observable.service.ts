import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

import { IApiService } from 'src/service/api.service.interface';
import { IObservableServiceLogin } from './observable.service.interface';
import { Login } from 'src/model/login';
import { Token } from 'src/model/token';

/**
 * Service managing observable objects.
 * (for items)
 */
@Injectable({
    providedIn: 'root'
})
export class ObservableServiceLogin implements IObservableServiceLogin {

    constructor(private apiService: IApiService) {}

    connection(path: string, login: Login): Promise<Token> {
        return this.apiService.post<Token>(path, login);
    }

    /**
     * Extract JSON data from response.
     *
     * @returns JSON data
     */
    private extractData(res: any) {
        return res;
    }
    /**
     * Handle error.
     *
     * @returns exception
     */
    private handleError(error: Response | any) {
        return Observable.throw('Error calling the url');
    }
}