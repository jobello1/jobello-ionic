import { HttpParams } from '@angular/common/http';

import { Login } from 'src/model/login';
import { Token } from 'src/model/token';

export abstract class IObservableServiceLogin {

    /**
     * Get a profile
     */
    abstract connection(path: string, login: Login): Promise<Token>;
}