import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { IApiService } from 'src/service/api.service.interface';
import { ApiService } from 'src/service/api.service';
import { QuestionService } from './../service/question.service';
import { AnswerService } from './../service/answer.service';
import {HTTP} from '@ionic-native/http/ngx';
import { RequestService } from './../service/request.sevice';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    QuestionService,
    RequestService,
    HTTP,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: IApiService, useClass: ApiService },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
