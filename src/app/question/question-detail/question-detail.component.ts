import { StudentProfile } from 'src/model/student_profile';
import { ProfileService } from 'src/service/profile.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ActionSheetController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { NavController, ToastController} from '@ionic/angular';

import { Answer } from 'src/model/answer';
import { Question } from 'src/model/question';

import { QuestionService } from './../../../service/question.service';
import { RequestService } from 'src/service/request.sevice';
import { UserService } from './../../../service/user.service';
import { Request } from 'src/model/request';

@Component({
  selector: 'app-question-detail',
  templateUrl: './question-detail.component.html',
  styleUrls: ['./question-detail.component.scss'],
})
export class QuestionDetailComponent implements OnInit {

  question:       Question;
  questionId:     number;
  private sub:    any;
  answerText:     string;
  userCanAnswer:  boolean;
  isStudent:      boolean;
  profile:        StudentProfile;

  constructor(private actionSheetController: ActionSheetController, private navCtrl: NavController, private route: ActivatedRoute, private toastController: ToastController, private profileService: ProfileService, private questionService: QuestionService, private requestService: RequestService, private userService: UserService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.questionId = params['id'];
      this.question = this.questionService.getQuestion(this.questionId);
      this.userCanAnswer = this.canAnswer();
    });
    this.profile = this.profileService.getCurrentProfile();
    this.isStudent = (this.profile.role == Role.student);
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Actions',
      buttons: [{
        text: 'Voir le profil',
        icon: 'eye',
        handler: () => {
          this.navCtrl.navigateForward("/profile/"+this.question.answer.author.role+"/"+this.question.answer.author.id);
        }
      }, {
        text: 'Demande de contact',
        icon: 'share-alt',
        handler: () => {
          this.requestService.addRequest(new Request(this.requestService.lengthRequest()+1, new Date(), this.profile, this.question.answer.author,false));
          this.sendRequestToast();
        }
      }]
    });
    await actionSheet.present();
  }

  async sendRequestToast() {
    const toast = await this.toastController.create({
      message: 'Votre demande a été envoyée.',
      duration: 2000,
      showCloseButton: true,
      closeButtonText: 'Fermer'
    });
    toast.present();
  }

  navBack(){
    this.navCtrl.back();
  }

  canAnswer(){
    return this.userService.canAnswer(this.question);
  }

  goAuthorProfile() {
    this.navCtrl.navigateForward("/profile/"+this.question.author.role+"/"+this.question.author.id);
  }

  sendAnswer(){
    let answer = new Answer(0,this.answerText, this.profileService.getProProfile(0), new Date())
    this.question.answer = answer;
    this.questionService.addAnswer(this.questionId,answer);
  }

}
