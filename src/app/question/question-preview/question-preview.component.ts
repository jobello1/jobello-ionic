import { Router } from '@angular/router';
import { QuestionService } from './../../../service/question.service';
import { Question } from './../../../model/question';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-question-preview',
  templateUrl: './question-preview.component.html',
  styleUrls: ['./question-preview.component.scss'],
})
export class QuestionPreviewComponent implements OnInit {

  @Input()
  question: Question;

  constructor(private questionService : QuestionService, private router: Router) { }

  ngOnInit() {
  }

}
