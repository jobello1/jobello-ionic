import { JobPageModule } from './../job/job.module';
import { Routes, RouterModule } from '@angular/router';
import { QuestionPreviewComponent } from './question-preview/question-preview.component';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QuestionPageRoutingModule } from './question-routing.module';

import { QuestionPage } from './question.page';
import { QuestionDetailComponent } from './question-detail/question-detail.component';
import { QuestionCreationFormComponent } from './question-creation-form/question-creation-form.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QuestionPageRoutingModule,
    JobPageModule
  ],
  declarations: [QuestionPage, QuestionDetailComponent, QuestionPreviewComponent, QuestionCreationFormComponent]
})
export class QuestionPageModule {}
