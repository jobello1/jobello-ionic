import { QuestionCreationFormComponent } from './question-creation-form/question-creation-form.component';
import { QuestionDetailComponent } from './question-detail/question-detail.component';
import { QuestionPreviewComponent } from './question-preview/question-preview.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfilePage } from './../profile/profile.page';
import { QuestionPage } from './question.page';

const routes: Routes = [
  {
    path: '',
    component: QuestionPage
  },
  {
    path: 'me',
    component: QuestionPage
  },
  {
    path: 'job/:id',
    component: QuestionPage
  },
  {
    path: 'list',
    component: QuestionPreviewComponent
  },
  {
    path: 'detail/:id',
    component: QuestionDetailComponent
  },
  {
    path: 'create/:jobId',
    component: QuestionCreationFormComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QuestionPageRoutingModule {}
