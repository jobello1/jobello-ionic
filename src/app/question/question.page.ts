import { NavController } from '@ionic/angular';
import { JobService } from './../../service/job.service';
import { UserService } from './../../service/user.service';
import { QuestionService } from './../../service/question.service';
import { Question } from './../../model/question';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Job } from 'src/model/job';

@Component({
  selector: 'app-question',
  templateUrl: './question.page.html',
  styleUrls: ['./question.page.scss'],
})
export class QuestionPage implements OnInit {

  questions: Array<Question>;
  jobId: number;
  private job: Job;
  private sub: any;
  titleSuffix: string;
  constructor(private questionService: QuestionService, private navCtrl: NavController, private userService : UserService, private jobService: JobService ,private route: ActivatedRoute) { }

  ngOnInit() {
    this.jobId = -1
    this.sub = this.route.params.subscribe(params => {
      this.jobId = params['id'];
      this.job = this.jobService.getJob(this.jobId);
      if(this.job){
        this.questions = this.questionService.getQuestionsByJob(this.job);
        this.titleSuffix = this.job.title;
      }else{
        this.questions = this.questionService.getMyQuestions();
        this.titleSuffix = "Mes questions";
      }
    });
  }

  allowedToCreateAnswer(){
    return this.userService.getRole() == Role.student;
  }

  navBack(){
    this.navCtrl.back();
  }

}
