import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionCreationFormComponent } from './question-creation-form.component';

describe('QuestionCreationFormComponent', () => {
  let component: QuestionCreationFormComponent;
  let fixture: ComponentFixture<QuestionCreationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionCreationFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionCreationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
