import { QuestionService } from './../../../service/question.service';
import { ProfileService } from './../../../service/profile.service';
import { NavController, ToastController } from '@ionic/angular';
import { JobService } from './../../../service/job.service';
import { Component, OnInit } from '@angular/core';
import { Job } from 'src/model/job';
import { Question } from 'src/model/question';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-question-creation-form',
  templateUrl: './question-creation-form.component.html',
  styleUrls: ['./question-creation-form.component.css']
})
export class QuestionCreationFormComponent implements OnInit {

  questionTitle: string;
  questionContent: string;
  selectedJob: Job;
  searchText: string;
  jobsToDisplay: Array<Job>;
  jobIdParameter: number;
  sub:any;

  constructor(private jobService: JobService ,private route: ActivatedRoute ,private navCtrl: NavController, private toastController: ToastController, private profileService: ProfileService, private questionService: QuestionService ) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.jobIdParameter = params['jobId'];
      if(this.jobIdParameter >= 0 ){
        this.selectedJob = this.jobService.getJob(this.jobIdParameter);
      }
    });
  }

  navBack(){
    this.navCtrl.back();
  }


  searchJob(){
    if(this.searchText.length >= 3){
      let jobs = this.jobService.getJobs();
      let jobsToDisplay = new Array<Job>();
      jobs.forEach( job => {
        if ( job.title.toUpperCase().includes(this.searchText.toUpperCase()) ) {
          jobsToDisplay.push(job);
        }
      });
      this.jobsToDisplay = jobsToDisplay;
    }
  }

  postAnswer(){
    if( !this.questionTitle || this.questionTitle.length < 10){
      this.presentToast('Veuillez renseigner un titre à votre question (10 caractères minimum)');
      return ;
    }
    if( !this.questionContent || this.questionContent.length < 10){
      this.presentToast('Veuillez ajouter des détails à votre question (10 caractères minimum)');
      return;
    }
    if(!this.selectedJob){
      this.presentToast('Veuillez sélectionner le métier auquel s\'adresse votre question');
      return;
    }
    let questionId = 0;
    if (this.questionService.getQuestions()){
      questionId = this.questionService.getQuestions().length;
    }
    let createdQuestion = new Question(questionId, this.questionTitle, this.questionContent, this.profileService.getStudentProfile(0), new Date(), this.selectedJob, null);
    this.questionService.postQuestion(createdQuestion);
    this.clearInput();
    this.presentToast('Votre question a bien été créée');
    this.navCtrl.navigateForward('/question/detail/'+questionId,{ replaceUrl: true });

  }

  private clearInput() {
    this.searchText = '';
    this.selectedJob = null;
    this.questionTitle = '';
    this.questionContent = '';
    this.jobsToDisplay = null;
  }

  //TODO: put all toast method in a service and call that service to display toast messages
  async presentToast(myMessage: string) {
    const toast = await this.toastController.create({
      message: myMessage,
      duration: 3000,
      showCloseButton: true,
      closeButtonText: 'Fermer'
    });
    toast.present();
  }
}
