import { NavController } from '@ionic/angular';
import { FavoriteJobService } from './../../service/favoriteJob.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.page.html',
  styleUrls: ['./favorites.page.scss'],
})
export class FavoritesPage implements OnInit {

  jobs;
  constructor(private favoriteJobService: FavoriteJobService, private navCtrl: NavController) { }

  ngOnInit() {
    this.jobs = this.favoriteJobService.getFavoriteJobs();
  }

  navBack(){
    this.navCtrl.back();
  }

}
