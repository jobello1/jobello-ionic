import { Component, OnInit } from '@angular/core';
import { ToastController, NavController } from '@ionic/angular';

import { Mail } from 'src/model/mail';
import { Request } from 'src/model/request';

import { MailService } from 'src/service/mail.service';
import { RequestService } from 'src/service/request.sevice';

@Component({
  selector: 'app-mail',
  templateUrl: './mail.page.html',
  styleUrls: ['./mail.page.scss'],
})
export class MailPage implements OnInit {

  mails:      Array<Mail>;
  requests:    Array<Request>;
  isMailsView:  boolean;

  constructor(private toastController: ToastController, private mailService: MailService, private requestService: RequestService, private navCtrl: NavController) { }

  ngOnInit() {
    this.mails = new Array<Mail>();
    this.mails = this.mailService.getMails();

    this.requests = new Array<Request>();
    this.requests = this.requestService.getRequests();

    this.isMailsView = true;
  }

  
  async presentToast() {
    const toast = await this.toastController.create({
      message: "Cette page n'est pas encore disponible...",
      duration: 2000,
      showCloseButton: true,
      closeButtonText: 'Fermer'
    });
    toast.present();
  }

  segmentChanged(e) {
    if(Object.values(e.detail)[0].toString() == "conversations")
      this.isMailsView = true;
    else
      this.isMailsView = false;
  }

  navBack(){
    this.navCtrl.back();
  }

}
