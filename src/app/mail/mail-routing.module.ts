import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MailPage } from './mail.page';
import { MailDetailComponent } from './mail-detail/mail-detail.component';

const routes: Routes = [
  {
    path: '',
    component: MailPage
  },
  {
    path: 'detail/:id',
    component: MailDetailComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MailPageRoutingModule {}
