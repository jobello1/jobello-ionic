import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MailPageRoutingModule } from './mail-routing.module';

import { MailPage } from './mail.page';
import { MailDetailComponent } from './mail-detail/mail-detail.component';
import { MailPreviewComponent } from './mail-preview/mail-preview.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MailPageRoutingModule
  ],
  declarations: [MailPage,MailDetailComponent,MailPreviewComponent]
})
export class MailPageModule {}
