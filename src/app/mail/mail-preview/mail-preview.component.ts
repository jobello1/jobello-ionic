import { UserService } from './../../../service/user.service';
import { Component, OnInit, Input } from '@angular/core';
import { NavController } from '@ionic/angular';

import { Mail } from 'src/model/mail';
import { Request } from 'src/model/request';
import { RequestService } from 'src/service/request.sevice';

@Component({
  selector: 'app-mail-preview',
  templateUrl: './mail-preview.component.html',
  styleUrls: ['./mail-preview.component.scss'],
})
export class MailPreviewComponent implements OnInit {

  @Input()
  mail: Mail;
  @Input()
  request: Request;

  formatLastMessage:  string;
  isMailView:         boolean;
  isProProfile:       boolean;

  constructor(private navCtrl: NavController, private requestService: RequestService, private userService: UserService) { }

  ngOnInit() {
    if(this.userService.getRole() == Role.pro)
      this.isProProfile = true;
    else
      this.isProProfile = false;

    if(this.mail != undefined) {
      this.isMailView = true;

      const lastMessageLength = 20;
      var messageCount = this.mail.messages.length;

      if(this.mail.messages.length > 0) {
        this.formatLastMessage = this.mail.messages[messageCount-1].content;

        if(this.mail.messages[messageCount-1].content != undefined) {

          if(this.formatLastMessage.length > lastMessageLength)
            this.formatLastMessage = this.formatLastMessage.substring(0,lastMessageLength)+"...";
        }
      } 
    }else{
      this.isMailView = false;
    }
  }

  acceptRequest(request) {
    this.requestService.acceptRequest(request);
    window.location.reload();
  }

  deleteRequest(request) {
    this.requestService.deleteRequest(request);
    window.location.reload();
  }

  goToDetails(id: number) {
    this.navCtrl.navigateForward("/mail/detail/"+id);
  }

}
