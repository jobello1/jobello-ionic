import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';

import { Message } from 'src/model/message';

import { Mail } from 'src/model/mail';
import { MailService } from 'src/service/mail.service';
import { UserService } from 'src/service/user.service';
import { ProfileService } from 'src/service/profile.service';

@Component({
  selector: 'app-mail-detail',
  templateUrl: './mail-detail.component.html',
  styleUrls: ['./mail-detail.component.scss'],
})
export class MailDetailComponent implements OnInit {

  mail:           Mail;
  currentMessage: string;

  constructor(private navCtrl: NavController, private activatedRoute: ActivatedRoute, private mailService: MailService, private profileService: ProfileService, private userService: UserService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.mail =  this.mailService.getMail(params['id']);
    });
  }

  backToMails() {
    this.navCtrl.navigateForward("/mail",{ replaceUrl: true });
  }

  sendMessage() {
    if(this.currentMessage.length > 0) {
      var profile;
      if(this.userService.getRole() == Role.student)
        profile = this.profileService.getStudentProfile(0);
      else
        profile = this.profileService.getProProfile(0);
      this.mail.messages.push(new Message(this.currentMessage, profile, this.mail.contact, "me"));
      this.mailService.setMail(this.mail);
      this.currentMessage="";
    }
  }
}
