import { Data } from './../model/data';
import { UserService } from './../service/user.service';
import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  public appPages ;
  connectionTitle: string;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private userService: UserService,
    private data: Data
  ) {
    this.initializeApp();
    this.initializeMenu(this.userService.getRole());
    this.data.roleObservable
      .subscribe((role) => {
      this.initializeMenu(role);
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  initializeMenu(role: Role) {
    if(this.userService.getToken().length == 0)
      this.connectionTitle = "Se connecter";
    else
      this.connectionTitle = "Se deconnecter";

    switch(role){
      case Role.student:
          this.appPages = [
            {
              title: 'Accueil',
              url: '/home',
              icon: 'home'
            },
            {
              title: 'Trouver un métier',
              url: '/domain',
              icon: 'compass'
            },
            {
              title: 'Questions',
              sub: [
                {
                  title: 'Poser une question',
                  url: '/question/create/-1'
                },
                {
                  title: 'Mes questions',
                  url: '/question/me'
                }
              ],
              icon: 'help'
            },
            {
              title: 'Messagerie',
              url: '/mail',
              icon: 'chatbubbles'
            },
            {
              title: 'Mon Profil',
              url: '/profile',
              icon: 'person'
            },
            {
              title: 'Favoris',
              url: '/favorites',
              icon: 'star'
            },
            {
              title: this.connectionTitle,
              url: '/login',
              icon: 'contact'
            }
          ];
          break;
      case Role.pro:
        this.appPages = [
          {
            title: 'Accueil',
            url: '/home',
            icon: 'home'
          },
          {
            title: 'Trouver un métier',
            url: '/domain',
            icon: 'compass'
          },
          {
            title: 'Questions',
            sub: [
              {
                title: 'Sur mon métier',
                url: '/question/job/0'
              }
            ],
            icon: 'help'
          },
          {
            title: 'Messagerie',
            url: '/mail',
            icon: 'chatbubbles'
          },
          {
            title: 'Mon Profil',
            url: '/profile',
            icon: 'person'
          },
          {
            title: 'Favoris',
            url: '/favorites',
            icon: 'star'
          },
          {
            title: this.connectionTitle,
            url: '/login',
            icon: 'contact'
          }
        ];

    }
  }
}
