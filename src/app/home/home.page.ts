import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ToastController } from '@ionic/angular';

import { Job } from 'src/model/job';
import { Domain } from './../../model/domain';

import { JobService } from './../../service/job.service';
import { DomainService } from './../../service/domain.service';
import { UserService } from './../../service/user.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private activatedRoute: ActivatedRoute, private userService: UserService, private toastController: ToastController, private jobService: JobService, private domainService: DomainService) {}
  randomJob: Job;
  randomDomain: Domain;

  ngOnInit() {
    this.randomDomain = this.getRandomDomain();
    this.randomJob = this.getRandomJob();
    console.log(this.randomJob);

    this.activatedRoute.params.subscribe(params => {
      if(params['student'] != null)
        this.presentToast('Vous êtes en mode lycéen');
      else if(params['pro'] != null)
        this.presentToast('Vous êtes en mode professionnel');
    });
  }

  getRandomJob(){
    return this.jobService.getRandomJob();
  }

  getRandomDomain(){
    return this.domainService.getRandomDomain();
  }

  //TODO: put all toast method in a service and call that service to display toast messages
  async presentToast(myMessage: string) {
    const toast = await this.toastController.create({
      message: myMessage,
      duration: 3000,
      showCloseButton: true,
      closeButtonText: 'Fermer'
    });
    toast.present();
  }

  clearData(){
    localStorage.clear();
  }

  switchToSchooler() {
    this.userService.setRole(Role.student);
  }

  switchToPro() {
    this.userService.setRole(Role.pro);
  }
}
