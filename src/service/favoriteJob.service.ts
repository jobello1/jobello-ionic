import { ToastController } from '@ionic/angular';
import { Job } from './../model/job';
import { Injectable } from '@angular/core';

import { Data } from './../model/data';
import { Mail } from './../model/mail';

@Injectable({
    providedIn: 'root'
})
export class FavoriteJobService {

    constructor(private data: Data, private toastController: ToastController) {}

    getFavoriteJobs() {
        return this.data.getMyFavoriteJobs();
    }

    addJobToFavorite(job: Job) {
        let jobs = this.data.getMyFavoriteJobs();
        let isAlreadyFavorite = false;
        jobs.forEach(j => {
            if(job.id === j.id) { 
               isAlreadyFavorite = true;
            }
        });
        if(!isAlreadyFavorite){
            jobs.push(job);
            this.data.setmyFavoriteJobs(jobs);
            this.presentToast("Le métier a été ajouté à vos favoris.");
        }else{
            this.presentToast("Une erreur est survenue.");
        }
       
    }

    removeJobFromFavorite(job: Job) {
        let jobs = this.data.getMyFavoriteJobs();
        jobs = jobs.filter(j => {
            return j.id !== job.id;
        });
        this.data.setmyFavoriteJobs(jobs);
        this.presentToast("Le métier a été retiré de vos favoris.");
    }

    isJobInFavorite(job: Job) {
        const jobs = this.data.getMyFavoriteJobs();
        let result = false;
        jobs.forEach(j => {
            if(job.id == j.id) { 
                result = true;
            }
        });
        return result;
    }

    async presentToast(myMessage: string) {
        const toast = await this.toastController.create({
          message: myMessage,
          duration: 3000,
          showCloseButton: true,
          closeButtonText: 'Fermer'
        });
        toast.present();
      }

}