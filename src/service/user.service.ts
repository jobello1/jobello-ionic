import { Data } from './../model/data';
import { Question } from './../model/question';

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private data: Data) {}

  getRole(){
    return this.data.getRole();
  }

  canAnswer(question: Question){
    //TODO: check if pro job is equal to the question job before returning true
    return this.data.getRole() === Role.pro ;
  }

  setRole(role: Role) {
    this.data.setRole(role);
  }
  
  getToken() {
    return this.data.getToken();
  }

  setToken(token: string) {
    this.data.setToken(token);
  }

}
