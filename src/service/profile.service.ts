import { Injectable } from '@angular/core';

import { Data } from 'src/model/data';
import { ProProfile } from 'src/model/pro_profile';
import { StudentProfile } from 'src/model/student_profile';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

    constructor(private userService: UserService, private data: Data) {}

    getAge(birthdate: Date) {
        var age = -1;
        const currentDate = new Date();
        const birthdayDate = new Date(birthdate);

        if(birthdayDate.getDate() <= currentDate.getDate() && birthdayDate.getMonth() <= currentDate.getMonth())
            age = 0;
        age += currentDate.getFullYear() - birthdayDate.getFullYear();

        return age;
    }

    public getStudentProfile(id: number){
        return this.data.getStudentProfile(id);
    }

    public getProProfile(id: number){
        return this.data.getProfessionalProfile(id);
    }

    public getCurrentProfile(){
        var profile;
        switch(this.userService.getRole()){
            case Role.student:
                profile = this.data.getCurrentStudentProfile();
                break;
            case Role.pro:
                profile = this.data.getCurrentProfessionalProfile();
                break;
        }
        return profile;
    }

    public setProProfile(profile: ProProfile) {
        this.data.setProProfile(profile);
    }

    public setStudentProfile(profile: StudentProfile) {
        this.data.setStudentProfile(profile);
    }
}