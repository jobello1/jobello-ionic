import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { IApiService } from './api.service.interface';
import { HTTP } from '@ionic-native/http/ngx';
import { throwError } from 'rxjs';

/**
 * Service responsable to make HTTP requests to all  plugins
 *
 */
@Injectable()
export class ApiService  {

  headers: any;

  constructor(
    private http: HttpClient,
    private httpNative: HTTP
  ) {
    this.headers = {'Access-Control-Allow-Headers': 'Accept, Content-Type', 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'};
  }


  private formatErrors(error: any) {
    return  throwError(error.error);
  }

  get(path: string, params: HttpParams = new HttpParams()) {
    const apiPath = `${path}`;
    const headers = this.headers;
    return this.httpNative.get(apiPath, {},{ headers });
  }

  put(path: string, body: any)  {
    const apiPath = `${path}`;
    const headers = this.headers;
    return this.httpNative.put(apiPath, JSON.stringify(body), { headers });
  }

  post(path: string, body: any)  {
    const apiPath = `${path}`;
    const headers = this.headers;
    console.log("post", headers);
    return this.httpNative.post(apiPath, body, {['Access-Control-Allow-Headers']: 'Accept, Content-Type', ['Accept']: 'application/json',['Content-Type']: 'application/json', ['Access-Control-Allow-Origin']: '*'});
  }

  delete(path) {
    const apiPath = `${path}`;
    const headers = this.headers;
    return this.httpNative.delete(apiPath,{} , { headers });
  }

}