import { HttpParams } from '@angular/common/http';

/**
 * Service responsable to make HTTP requests to all  plugins
 *
 */
export abstract class IApiService {
    /**
     * Performs a request with `get` http method.
     *
     * @param path path to be called
     * @param params Request params
     */
    abstract get<T>(path: string, params?: HttpParams): Promise<T>;

    /**
     * Performs a request with `post` http method.
     *
     * @param path path to be called
     * @param body Request body
     */
    abstract post<T>(path: string, body: any): Promise<T>;

    /**
     * Performs a request with `put` http method.
     *
     * @param path path to be called
     * @param body Request body
     */
    abstract put<T>(path: string, body: any): Promise<T>;

    /**
     * Performs a request with `delete` http method.
     *
     * @param path path to be called
     */
    abstract delete<T>(path: string): Promise<T>;
}
