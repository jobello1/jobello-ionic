import { Injectable } from '@angular/core';

import { Data } from 'src/model/data';
import { Request } from 'src/model/request';
import { Mail } from 'src/model/mail';
import { Message } from 'src/model/message';

@Injectable()
export class RequestService {

    constructor(private data: Data) {}

    public acceptRequest(request: Request) {
        this.data.acceptRequest(request);
        this.data.setMails(new Mail(
            0,
            request.contact,
            [
                new Message(request.contact.firstname+" a accepté votre demande de contact, envoyer lui un message !", request.sender, request.contact, "contact"),
            ],
            new Date()
        ));
    }

    public addRequest(request: Request) {
        this.data.addRequest(request);
    }

    public deleteRequest(request: Request) {
        this.data.deleteRequest(request);
        window.location.reload();   
    }

    public lengthRequest() {
        return this.data.lenghtRequest();
    }

    public getRequests() {
        return this.data.getRequests()
    }
}