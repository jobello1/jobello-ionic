import { Answer } from 'src/model/answer';
import { Data } from './../model/data';
import { AnswerService } from './answer.service';
import { Question } from './../model/question';

import { Injectable } from '@angular/core';
import { StudentProfile } from 'src/model/student_profile';
import { ProfileNote } from 'src/model/profile_note';
import { Job } from 'src/model/job';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  constructor(private answerService: AnswerService, private data: Data) {
  }

  getQuestion(id: number) {
    return this.data.getQuestions()[id];
  }

  getQuestions(){
    return this.data.getQuestions();
  }
  
  getQuestionsByJob(job: Job){
    let questions =  this.data.getQuestions();
    let result= [];
    questions.forEach(question => {
      if(question.job.id == job.id){
        result.push(question);
      }
    });
    return result;
  }

  getMyQuestions(){
    return this.data.getMyQuestions();
  }

  postQuestion(createdQuestion: Question){
    let questions = this.data.getQuestions();
    questions.push(createdQuestion);
    let myQuestions = this.data.getMyQuestions();
    myQuestions.push(createdQuestion);
    this.data.setQuestions(questions);
    this.data.setMyQuestions(myQuestions);
  }

  addAnswer(questionId: number, answer: Answer){
    let questions = this.getQuestions();
    for(let index = 0; index < questions.length; index ++){
      let question = questions[index];
      if(question.id == questionId){
        question.answer = answer;
        questions[index] = question;
        this.data.setQuestions(questions);
        this.data.getMyQuestions()[this.data.getMyQuestions().indexOf(question)];
        this.data.setMyQuestions(this.data.getMyQuestions());
      }
    }

  }
}
