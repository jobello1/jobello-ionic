import { Domain } from './../model/domain';
import { Data } from './../model/data';
import { Answer } from './../model/answer';
import { AnswerService } from './answer.service';
import { Question } from './../model/question';

import { Injectable } from '@angular/core';
import { StudentProfile } from 'src/model/student_profile';
import { ProfileNote } from 'src/model/profile_note';
import { Job } from 'src/model/job';

@Injectable({
  providedIn: 'root'
})
export class DomainService {

  constructor(private data: Data) {
  }

  getDomains() {
    return this.data.getDomains();
  }

  getDomain(id: number){
    return this.data.getDomains()[id];
  }

  getDomainJobsMapByFirstLetter(domain: Domain){
    let jobsToDisplayByLetter = new Map<string,Array<Job>>();
    domain.jobs.forEach(job => {
      let firstLetterOfTheJobName = job.title[0];
      let jobsOfTheLetter =  jobsToDisplayByLetter.get(firstLetterOfTheJobName);
      if(jobsOfTheLetter == null){
        jobsOfTheLetter = new Array<Job>();
      }
      jobsOfTheLetter.push(job);
      jobsToDisplayByLetter.set(firstLetterOfTheJobName,jobsOfTheLetter);      
    });
    return jobsToDisplayByLetter;
  }

  getRandomDomain(){
    let domains = this.data.getDomains();
    let randomIndex = Math.floor(Math.random()*domains.length) ;
    return domains[randomIndex];
  }
}
