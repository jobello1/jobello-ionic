import { Injectable } from '@angular/core';

import { Data } from './../model/data';
import { Mail } from './../model/mail';

@Injectable({
    providedIn: 'root'
})
export class MailService {

    constructor(private data: Data) {}

    getMail(id: number) {
        return this.data.getMail(id);
    }

    getMails() {
        return this.data.getMails();
    }

    setMail(mail: Mail) {
        this.data.setMails(mail);
    }
}