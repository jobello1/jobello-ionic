import { Question } from './../model/question';

import { Injectable } from '@angular/core';
import { StudentProfile } from 'src/model/student_profile';
import { ProfileNote } from 'src/model/profile_note';
import { Answer } from 'src/model/answer';
import { ProProfile } from 'src/model/pro_profile';

@Injectable({
  providedIn: 'root'
})
export class AnswerService {

  constructor() { 
  }

  getAnswer(id : number){
      let answer = new Answer(
          1,
          "Le métier d'assistant commercial consiste à assurer le bon fonctionnement du commerce au sein d'une agence. Le poste peut avoir plusieurs facettes comme rédiger et remplir des documents officiels (actes de vente, bails) mais aussi assurer une bonne communication entre les commerciaux.",
          new ProProfile(
              1,
              "Constant","Alain",new Date('04/06/1986'),
              "https://miro.medium.com/max/10368/1*Zi7t4KAPyvFgnDs1Uojyag.jpeg",
              new ProfileNote("Ma vision des choses","Positive energy brings positive things!"),
              "Nord-Pas-De-Calais",
              "Assistant commercial"
          ),
          new Date()
         
          );
      return answer;
  }

}