import { Data } from '../model/data';

import { Injectable } from '@angular/core';
import { Job } from 'src/model/job';

@Injectable({
  providedIn: 'root'
})
export class JobService {

  constructor(private data: Data) {
  }

  getJob(id: number) {
    return this.data.getJobs()[id];
  }
  
  getJobs(){
    return this.data.getJobs();
  }

  getRandomJob(){
    let jobs = this.data.getJobs();
    let randomIndex = Math.floor(Math.random()*jobs.length) ;
    return jobs[randomIndex];
  }

}
